package in.net.hanzapdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;



import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class PostFormActivity extends Activity implements  View.OnClickListener {
    private Toolbar toolbar;
    EditText postCategories,paymentMethod,postLocation,postDate,jobTerm,rate;

    TextView selection,category;
    Spinner currency;
    Calendar myCalendar = Calendar.getInstance();
    private ConstraintLayout constraintLayout;
    private String payementmethod,Rate;
    private ProgressDialog loading;
    Intent intent;
    private int postSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_form);
        intent=getIntent();



        postCategories = findViewById(R.id.et_postCategory);
        paymentMethod = findViewById(R.id.et_postPaymentMethod);
        postLocation = findViewById(R.id.et_postLocation);
        postDate = findViewById(R.id.et_postDate);
        jobTerm = findViewById(R.id.et_postJobTerm);
        rate=findViewById(R.id.et_postRate);
        constraintLayout = findViewById(R.id.constraintLayout_category);
        toolbar = findViewById(R.id.custom_toolbar);
        selection=findViewById(R.id.tv_title_2);
        category = findViewById(R.id.tv_title);



        category.setText("New Post");
        selection.setText("Post");
        postCategories.setOnClickListener(this);
        paymentMethod.setOnClickListener(this);
        postLocation.setOnClickListener(this);
        rate.setOnClickListener(this);

        jobTerm.setOnClickListener(this);

        if(intent!= null)
        {
            postSelected = getIntent().getIntExtra("selection",0);
            postCategories.setText(postSelected+"\t categories selected");

        }
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };
        postDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(PostFormActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        selection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loading = ProgressDialog.show(PostFormActivity.this, "", "Please wait...", false, false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loading.dismiss();
                    }
                },15000);
            }
        });
    }


    @Override
    public void onClick(View v) {
        int id= v.getId();
        int selection=0;
        switch (id)
        {

            case R.id.et_postCategory:
                startActivity(new Intent(PostFormActivity.this, PostCategoryActivity.class));
                overridePendingTransition(R.anim.layout_animation_start, R.anim.layout_animation_end);
                break;
            case R.id.et_postPaymentMethod:
                payementmethod="No Preference";
                final String[] paymentmethod = {"No Preference","E-Payment","Cash"};

                AlertDialog.Builder alert1 = new AlertDialog.Builder(this);

                alert1.setTitle("Job Term");

                alert1.setPositiveButton("Select", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        PaymentMethod(payementmethod);
                        dialog.dismiss();


                    }
                });

                if(payementmethod.equals("No Preference"))
                {
                    selection=0;
                }else  if(payementmethod.equals("E-Payment"))
                {
                    selection=1;
                }else  if(payementmethod.equals("E-Payment"))
                {
                    selection=2;
                }


                alert1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });
                alert1.setSingleChoiceItems(paymentmethod,selection, new

                        DialogInterface.OnClickListener()

                        {

                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                if(paymentmethod[which]=="No Preference")

                                {
                                    payementmethod="No Preference";
//                                    PaymentMethod("No Preference");

                                }

                                else if (paymentmethod[which]=="E-Payment")

                                {
                                    payementmethod="E-Payment";
//                                    PaymentMethod("E-Payment");

                                }

                                else if (paymentmethod[which]=="Cash")

                                {
                                    payementmethod="Cash";
//                                    PaymentMethod("Cash");

                                }
                            }

                        });
                alert1.show();
                break;
            case R.id.et_postLocation:

                startActivity(new Intent(PostFormActivity.this, PostLocationActivity.class));
                overridePendingTransition(R.anim.layout_animation_start, R.anim.layout_animation_end);
                break;

            case R.id.et_postJobTerm:
                final String[] jobtype = {"Recurring Job","Same Day Job","Multiday Job"};

                AlertDialog.Builder alert = new AlertDialog.Builder(this);

                alert.setTitle("Job Term");


                alert.setPositiveButton("Select", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });


                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert.setSingleChoiceItems(jobtype,0, new

                        DialogInterface.OnClickListener()

                        {

                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                if(jobtype[which]=="Recurring Job")

                                {
                                    setJobtype("Recurring Job");

                                }

                                else if (jobtype[which]=="Same Day Job")

                                {

                                    setJobtype("Same Day Job");

                                }

                                else if (jobtype[which]=="Multiday Job")

                                {

                                    setJobtype("Multiday Job");

                                }
                            }

                        });
                alert.show();
                break;

            case R.id.et_postRate:
                final String[] rateType = {"No Preference","Fixed Budget","Hourly Rate"};
                createAlertDailog(rateType);
                break;
        }

    }

    private void setJobtype(String recurring_job) {

        jobTerm.setText(recurring_job);
    }


    private void PaymentMethod(String recurring_job) {

        paymentMethod.setText(recurring_job);
    }
    private void updateLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        postDate.setText(sdf.format(myCalendar.getTime()));
    }

    private void createAlertDailog(final String[] items){
        int selection=0;
        AlertDialog.Builder alert1 = new AlertDialog.Builder(this);

        alert1.setTitle("Job Term");

        alert1.setPositiveButton("Select", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                PaymentRate(Rate);
                dialog.dismiss();


            }
        });




        alert1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });
        alert1.setSingleChoiceItems(items,selection, new

                DialogInterface.OnClickListener()

                {

                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        if(items[which]==items[0])

                        {
                            Rate=items[which];
//                                    PaymentMethod("No Preference");

                        }

                        else if (items[which]==items[1])

                        {
                            Rate=items[which];
//                                    PaymentMethod("E-Payment");

                        }

                        else if (items[which]==items[2])

                        {
                            Rate=items[which];
//                                    PaymentMethod("Cash");

                        }
                    }

                });
        alert1.show();
    }

    private void PaymentRate(final String ratetype) {
        rate.setText(ratetype);
    }
}


