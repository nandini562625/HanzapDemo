package in.net.hanzapdemo;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class GridViewAdapter extends BaseAdapter {

    private Context context;
    private Integer[] imageAsset;
    public List<Integer> selection= new ArrayList<>();
    TextView navigation;


    public GridViewAdapter(Context postCategoryActivity, Integer[] imageAsset, TextView navigation) {

        context = postCategoryActivity;
        this.imageAsset = imageAsset;
        this.navigation=navigation;

    }

    @Override
    public int getCount() {
        return imageAsset.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {



        navigation.setText(selection.size()+"\tselected");

        View grid;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            grid = new View(context);
            grid = inflater.inflate(R.layout.grid_items_layout, null);
            TextView textView = (TextView) grid.findViewById(R.id.tvOverlayText);
            final ImageView imageView = (ImageView)grid.findViewById(R.id.ivBackground);
            imageView.setImageResource(imageAsset[position]);


            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(selection.size()!=3)
                    {
                        if(selection.contains(position)){

                            try{
                                selection.remove(position);

                                final ObjectAnimator oa1 = ObjectAnimator.ofFloat(imageView, "scaleX", 1f, 0f);
                                final ObjectAnimator oa2 = ObjectAnimator.ofFloat(imageView, "scaleX", 0f, 1f);
                                oa1.setInterpolator(new DecelerateInterpolator());
                                oa2.setInterpolator(new AccelerateDecelerateInterpolator());
                                oa1.addListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        imageView.setImageResource(R.drawable.emma);
                                        oa2.start();
                                    }
                                });
                                oa1.start();
                            }
                            catch (IndexOutOfBoundsException e){
                                Log.d("TAG", "onClick: "+e);
                            }

                        }else {
                            selection.add(position);

                            final ObjectAnimator oa1 = ObjectAnimator.ofFloat(imageView, "scaleX", 1f, 0f);
                            final ObjectAnimator oa2 = ObjectAnimator.ofFloat(imageView, "scaleX", 0f, 1f);
                            oa1.setInterpolator(new DecelerateInterpolator());
                            oa2.setInterpolator(new AccelerateDecelerateInterpolator());
                            oa1.addListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    imageView.setImageResource(R.drawable.material_button_gradient2);
                                    oa2.start();
                                }
                            });
                            oa1.start();
                        }
                    }
                    else if(selection.size()==3)
                    {
                        Toast.makeText(context,"you have reached maximum selection",Toast.LENGTH_SHORT).show();

                    }
                }
            });
        } else {
            grid = (View) convertView;
        }

        return grid;

    }
}
