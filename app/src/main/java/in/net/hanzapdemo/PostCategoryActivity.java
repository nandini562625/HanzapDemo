package in.net.hanzapdemo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class PostCategoryActivity extends Activity {

    private TextView navigation,selection,category;
    private GridView gridView;
    private GridViewAdapter adapter;
    private List<Integer> results = new ArrayList<>();
    private int count=0;
    private Toolbar toolbar;
    private ProgressDialog loading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_category);

        navigation = findViewById(R.id.navigation);
        toolbar = findViewById(R.id.custom_toolbar);
        gridView = findViewById(R.id.category);

        selection=findViewById(R.id.tv_title_2);
        category = findViewById(R.id.tv_title);


        category.setText("Post Categories");
        selection.setText("Select");
        Integer[] imageAsset = {
                R.drawable.emma, R.drawable.emma, R.drawable.emma,
                R.drawable.emma, R.drawable.emma, R.drawable.emma,
                R.drawable.emma, R.drawable.emma, R.drawable.emma,
                R.drawable.emma, R.drawable.emma, R.drawable.emma,
                R.drawable.emma, R.drawable.emma, R.drawable.emma,
                R.drawable.emma, R.drawable.emma, R.drawable.emma,
                R.drawable.emma, R.drawable.emma, R.drawable.emma,

        };


        navigation.setText(count+"\tselected");

        adapter = new GridViewAdapter(PostCategoryActivity.this, imageAsset,navigation);
        gridView.setAdapter(adapter);
        selection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getApplicationContext(), PostFormActivity.class).putExtra("selection",adapter.selection.size()));
                overridePendingTransition(R.anim.layout_animation_start, R.anim.layout_animation_end);
                finish();


            }
        });


    }
}
